<?php

namespace Drupal\commerce_elavon;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Maps AVS responses to their labels.
 */
class AvsResponses implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * An associative array of translated AVS response labels, keyed by code.
   *
   * @var \Drupal\Core\StringTranslation\TranslatableMarkup[]
   */
  protected static $labels = [];

  /**
   * Creates a new object.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(TranslationInterface $string_translation) {
    $this->setStringTranslation($string_translation);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('string_translation')
    );
  }

  /**
   * Returns the label for an AVS response code.
   *
   * @param $avs_code
   *
   * @return string
   */
  public function getLabel($avs_code) {
    $this->ensureLabels();
    if (!isset(static::$labels[$avs_code])) {
      throw new \InvalidArgumentException(sprintf("Unexpected AVS code '%s'", $avs_code));
    }
    return static::$labels[$avs_code];
  }

  /**
   * Sets up the translated labels on the first call only.
   */
  protected function ensureLabels() {
    if (static::$labels) {
      return;
    }
    static::$labels = [
      'A' => $this->t("Address matches - ZIP Code does not match"),
      'B' => $this->t("Street address match, Postal code in wrong format (international issuer)"),
      'C' => $this->t("Street address and postal code in wrong formats"),
      'D' => $this->t("Street address and postal code match (international issuer)"),
      'E' => $this->t("AVS Error"),
      'F' => $this->t("Address does compare and five-digit ZIP code does compare (UK only)"),
      'G' => $this->t("Service not supported by non-US issuer"),
      'I' => $this->t("Address information not verified by international issuer"),
      'M' => $this->t("Street Address and Postal code match (international issuer)"),
      'N' => $this->t("No Match on Address (Street) or ZIP"),
      'O' => $this->t("No Response sent"),
      'P' => $this->t("Postal codes match, Street address not verified due to incompatible formats"),
      'R' => $this->t("Retry, System unavailable or Timed out"),
      'S' => $this->t("Service not supported by issuer"),
      'U' => $this->t("Address information is unavailable"),
      'W' => $this->t("9-digit ZIP matches, Address (Street) does not match"),
      'X' => $this->t("Exact AVS Match"),
      'Y' => $this->t("Address (Street) and 5-digit ZIP match"),
      'Z' => $this->t("5-digit ZIP matches, Address (Street) does not match"),
    ];
  }

}
