README file for Commerce Elavon

CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Installation
* Configuration
* How It Works
* Troubleshooting
* Maintainers

INTRODUCTION
------------
Integrate Elavon Virtual Merchant payment service Converge payment gateway
with Commerce module.
There are two payment options.
* Onsite
   - It support sales transaction, real-time authorization for a credit card
     transaction, capture the authorized amount(include partial), refund
     transaction(include partial).
* Offsite Redirect
   - Add a new offsite redirect payment. It only supports credit card transaction.
     No credit card number will ever need to pass through the web server.

REQUIREMENTS
------------
This module requires the following:
* Submodules of Drupal Commerce package (https://drupal.org/project/commerce)
  - Commerce core
  - Commerce Payment (and its dependencies)
* Elavon Merchant account (https://www.elavon.com/)

INSTALLATION
------------
* This module needs to be installed via Composer, which will download
the required libraries.
composer require "drupal/commerce_elavon"
https://www.drupal.org/docs/8/extending-drupal-8/installing-modules-composer-dependencies

CONFIGURATION
-------------
* Create a new Elavon payment gateway.
  Administration > Commerce > Configuration > Payment gateways > Add payment gateway
  It is recommended to enter test credentials and then override these with live
  credentials in settings.php. This way, live credentials will not be stored in the db.

HOW IT WORKS
------------
* General considerations:
  - The store owner must have a Elavon merchant account.
    Sign up here:
    https://www.elavon.com

Note about credit card acceptance
------------------------------
Elavon accepts all major credit card brands.
- https://www.elavon.be/products/card-acceptance.html

Test Cards
----------
- https://developer.elavon.com/content/home/test_cards/test_cards.html
