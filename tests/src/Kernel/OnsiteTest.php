<?php

namespace Drupal\Tests\commerce_elavon\Kernel;

/**
 * Provides tests for the Onsite Elavon payment gateway.
 *
 * @group commerce_elavon
 *
 * @coversDefaultClass \Drupal\commerce_elavon\Plugin\Commerce\PaymentGateway\Onsite
 */
class OnsiteTest extends OnsiteKernelTestBase {

  /**
   * Override the plugin to be able to mock the API request and response.
   */
  protected const PLUGIN_ID = 'elavon_onsite_test';

  /**
   * The onsite gateway with a mocked client.
   *
   * @var \Drupal\commerce_elavon_test\Plugin\Commerce\PaymentGateway\TestOnsiteInterface
   */
  protected $onsite;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'commerce_elavon_test',
  ];

  /**
   * Smoke tests creating a payment.
   */
  public function testCreatePaymentMethod(): void {
    $form_params = [
      'xmldata' => $this->getRequestXmldata('ccgettoken.xml'),
    ];
    $this->onsite->request(NULL, $form_params)
      ->willReturn($this->getResponse('ccgettoken/success.xml'));
    $this->onsite->createPaymentMethod($this->paymentMethod, $this->paymentMethodDetails);

    $this->assertSame('4692536310000002', $this->paymentMethod->getRemoteId());
    $this->assertSame(1675169999, $this->paymentMethod->getExpiresTime());
  }

  /**
   * Returns the contents of the XML request fixture.
   *
   * Note that it isn't the actual request, it's the data that's associated with
   * the xmldata parameter and posted to the endpoint.
   *
   * @param string $path
   *   The path to the file from the fixtures/response directory.
   *
   * @return string
   *   The file contents.
   */
  protected function getRequestXmldata(string $path): string {
    $contents = file_get_contents(__DIR__ . '/../../fixtures/request-xmldata/' . $path);
    if ($contents === FALSE) {
      throw new \InvalidArgumentException("Can't find request XML data '$path'");
    }
    // The data shouldn't have a trailing newline, but text editors will
    // typically add one, so remove it before comparing.
    return rtrim($contents, "\n\r");
  }

  /**
   * Returns the contents of the XML response fixture.
   *
   * @param string $path
   *   The path to the file from the fixtures/response directory.
   *
   * @return string
   *   The file contents.
   */
  protected function getResponse(string $path): string {
    $contents = file_get_contents(__DIR__ . '/../../fixtures/response/' . $path);
    if ($contents === FALSE) {
      throw new \InvalidArgumentException("Can't find response '$path'");
    }
    return $contents;
  }

}
