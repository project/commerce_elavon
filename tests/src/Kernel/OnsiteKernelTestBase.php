<?php

namespace Drupal\Tests\commerce_elavon\Kernel;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethod;
use Drupal\commerce_price\Price;
use Drupal\profile\Entity\Profile;
use Drupal\Tests\commerce_order\Kernel\OrderKernelTestBase;

/**
 * Provides a base class for Elavon onsite gateway kernel tests.
 */
abstract class OnsiteKernelTestBase extends OrderKernelTestBase {

  /**
   * The ID of the onsite plugin.
   */
  protected const PLUGIN_ID = 'elavon_onsite';

  /**
   * The payment gateway.
   *
   * @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface
   */
  protected $gateway;

  /**
   * The onsite plugin.
   *
   * @var \Drupal\commerce_elavon\Plugin\Commerce\PaymentGateway\OnsiteInterface
   */
  protected $onsite;

  /**
   * The test order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * The test payment method.
   *
   * @var \Drupal\commerce_payment\Entity\PaymentMethodInterface
   */
  protected $paymentMethod;

  /**
   * The test payment method details.
   *
   * @var array
   */
  protected $paymentMethodDetails;

  /**
   * The test billing profile.
   *
   * @var \Drupal\profile\Entity\ProfileInterface
   */
  protected $profile;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'commerce_elavon',
    'commerce_payment',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('commerce_payment');
    $this->installEntitySchema('commerce_payment_method');
    $this->installEntitySchema('commerce_payment_gateway');
    $this->installConfig(['commerce_payment', 'commerce_elavon']);

    $this->gateway = PaymentGateway::create([
      'id' => 'onsite',
      'label' => 'On-site',
      'plugin' => static::PLUGIN_ID,
    ]);
    $this->gateway->save();

    $this->onsite = $this->gateway->getPlugin();

    $configuration = ['mode' => 'test'] + $this->getOnsiteConfiguration();
    $this->onsite->setConfiguration($configuration);
    $this->profile = Profile::create([
      'type' => 'customer',
      'address' => [
        'country_code' => 'US',
        'postal_code' => '82941',
        'locality' => 'Pinedale',
        'address_line1' => '3 Cheyenne Trail',
        'address_line2' => '#"\'^`{|}&<>',
        'administrative_area' => 'WY',
        'given_name' => '以呂波耳',
        'family_name' => 'frànçAISisübeåw',
      ],
      'uid' => 0,
    ]);
    $this->profile->save();
    $this->paymentMethod = PaymentMethod::create([
      'type' => 'credit_card',
      'payment_gateway' => 'onsite',
      'billing_profile' => $this->profile,
    ]);
    $this->paymentMethod->save();

    $order_item = OrderItem::create([
      'type' => 'test',
      'quantity' => 1,
      'unit_price' => new Price('10', 'USD'),
    ]);
    $order_item->save();

    $this->order = Order::create([
      'type' => 'default',
      'store_id' => $this->store,
      'order_items' => [$order_item],
      'state' => 'draft',
      'payment_gateway' => 'onsite',
    ]);
    $this->order->save();

    $this->paymentMethodDetails = [
      'type' => 'visa',
      'number' => '4000000000000002',
      'expiration' => ['month' => '01', 'year' => date('Y') + 1],
      'security_code' => '123',
    ];
  }

  /**
   * Asserts the state of a payment object.
   *
   * @param string $expected
   *   The expected state ID.
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment.
   */
  protected function assertPaymentState($expected, PaymentInterface $payment): void {
    $this->assertSame($expected, $payment->getState()->getId());
  }

  /**
   * Returns the onsite plugin configuration to use for the test.
   *
   * @return array
   */
  protected function getOnsiteConfiguration(): array {
    return [
      'merchant_id' => 'MERCHANT_ID',
      'user_id' => 'USER_ID',
      'pin' => 'PIN',
      'multicurrency' => FALSE,
    ];
  }

}
