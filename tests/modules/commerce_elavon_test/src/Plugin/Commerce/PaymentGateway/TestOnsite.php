<?php

namespace Drupal\commerce_elavon_test\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_elavon\Plugin\Commerce\PaymentGateway\Onsite;
use PHPUnit\Framework\ExpectationFailedException;

/**
 * Provides the test on-site payment gateway.
 *
 * I think a neater approach would be to use a discrete client like Guzzle and
 * mock that with eg. prophecy.
 *
 * @CommercePaymentGateway(
 *   id = "elavon_onsite_test",
 *   label = "Test Elavon (On-site)",
 *   display_label = "Elavon",
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_elavon\PluginForm\Onsite\PaymentMethodAddForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "dinersclub", "discover", "jcb", "maestro", "mastercard", "visa", "unionpay"
 *   },
 * )
 */
class TestOnsite extends Onsite implements TestOnsiteInterface {

  /**
   * @var array|null
   */
  protected $form_params;

  /**
   * The response contents.
   *
   * @var string|null
   */
  protected $testResponse;

  /**
   * @var string|null
   */
  protected $transaction_url;

  /**
   * {@inheritdoc}
   */
  public function request(?string $transaction_url, ?array $form_params): TestOnsiteInterface {
    $this->transaction_url = $transaction_url;
    $this->form_params = $form_params;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function willReturn(string $response): TestOnsiteInterface {
    $this->testResponse = $response;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  protected function post(string $transaction_url, array $form_params): string {
    if (!is_null($this->transaction_url) && $this->transaction_url !== $transaction_url) {
      $message = sprintf("Expected transaction URL '%s' but got '%s'.", $this->transaction_url, $transaction_url);
      throw new ExpectationFailedException($message);
    }

    if (!is_null($this->form_params) && $this->form_params !== $form_params) {
      $message = sprintf("Expected form_params\n%sbut got\n%s.", print_r($this->form_params, TRUE), print_r($form_params, TRUE));
      throw new ExpectationFailedException($message);
    }

    assert(!is_null($this->testResponse));
    return $this->testResponse;
  }

}
