<?php

namespace Drupal\commerce_elavon_test\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_elavon\Plugin\Commerce\PaymentGateway\OnsiteInterface;

/**
 * Provides an interface for the mock onsite payment gateway.
 */
interface TestOnsiteInterface extends OnsiteInterface {

  /**
   * Sets the expected transaction URL and form parameters for the request.
   *
   * @param string $transaction_url
   *   The expected URL.
   * @param array $form_params
   *   The expected form parameters.
   *
   * @return self
   */
  public function request(?string $transaction_url, ?array $form_params): self;

  /**
   * Sets the contents to be returned by the next curl request.
   *
   * @param string $response
   *   The response contents.
   *
   * @return self
   */
  public function willReturn(string $response): self;

}
